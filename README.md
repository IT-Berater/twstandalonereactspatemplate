# TWStandaloneReactSPATemplate

A minimal (495 Byte) only Standalone React-App using React and Babel.

In first Terminal
1. npm install --save-dev babel-cli
2. npm run build

Result:
> standalone-react-template@0.0.1 build
> npx babel app.js --out-file app.min.js --presets minify --watch

Second Terminal
1. npm run start

Result:
> standalone-react-template@0.0.1 start
> open http://localhost:8888/ && python3 -m http.server 8888

<pre>
Serving HTTP on 0.0.0.0 port 8888 (http://0.0.0.0:8888/) ...
127.0.0.1 - - [16/Jun/2021 12:58:42] "GET / HTTP/1.1" 304 -
127.0.0.1 - - [16/Jun/2021 12:58:42] "GET /app.min.js HTTP/1.1" 200 -
127.0.0.1 - - [16/Jun/2021 12:58:42] code 404, message File not found
127.0.0.1 - - [16/Jun/2021 12:58:42] "GET /favicon.ico HTTP/1.1" 404 -
</pre>

Open:
http://localhost:8888/

Result:

Template SPA:
Seconds: 1....

Minimal Size:
<pre>
-rw-r--r--    1   staff     168 16 Jun 12:55 .babelrc
-rw-r--r--    1   staff     764 16 Jun 13:01 README.md
-rw-r--r--    1   staff     649 16 Jun 12:52 app.js
-rw-r--r--    1   staff     495 16 Jun 12:58 app.min.js
-rw-r--r--    1   staff     534 16 Jun 12:44 index.html
-rw-r--r--@   1   staff     717 16 Jun 12:57 package.json
</pre>
